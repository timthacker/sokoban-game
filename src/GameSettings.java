import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GameSettings extends JPanel{
	private static final long serialVersionUID = 1L;
	private String playerName;
	private StateNotifier notify;
	private Difficulty difficulty;

	public GameSettings(JFrame frame, StateNotifier s){
		super();
		notify = s;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JLabel label = new JLabel("Details");
		label.setFont(new Font("Sans Serif", Font.BOLD, 36));
		label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		this.add(label);
		this.add(Box.createRigidArea(new Dimension(0,10)));

		JTextField textField = new JTextField(20);
		Font font = new Font("Courier", Font.BOLD,36);
		textField.setFont(font);
		Dimension maxSize = new Dimension(200, 40);
		textField.setMaximumSize(maxSize);
		this.add(textField);
		this.add(Box.createRigidArea(new Dimension(0,10)));

		JLabel Difficulty = new JLabel("Difficulty");
		Difficulty.setFont(new Font("Sans Serif", Font.BOLD, 36));
		Difficulty.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		this.add(Difficulty);
		this.add(addAButton("EASY", this, frame, textField,Color.green));
		this.add(addAButton("HARD", this, frame, textField, Color.red));

		this.add(addAButton("BACK", this, frame, textField, Color.black));
		this.add(addAButton("START", this, frame, textField, Color.black));





		this.setOpaque(false);
		this.setBackground(Color.GRAY);
		this.setPreferredSize(new Dimension(400, 500));
	}

	private JButton addAButton(String text, JPanel panel, JFrame frame, JTextField textField,Color colour) {
		JButton button = new JButton(text);

		button.setBackground(colour);
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		Dimension maxSize = new Dimension(200, 25);
		button.setMaximumSize(maxSize);
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Sans Serif", Font.BOLD, 25));
		panel.add(button);
		panel.add(Box.createRigidArea(new Dimension(0,5)));
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(((JButton) e.getSource()).getText().equals("START")){
					//System.out.println("Test");
					//put more conditions for start, not if difficulty is selected
					//not if name isn't selected
					if(!textField.getText().isEmpty()){
						playerName=textField.getText();
						//get player name
						//System.out.println(playerName);
						//System.out.println(difficulty);
						notify.stateChanged(difficulty);
						//panel.removeAll();
						//panel.validate();
						//panel.repaint();
						//Game g= new Game(Difficulty, playerName);
					}



				}else if(((JButton) e.getSource()).getText().equals("EASY")){
					//System.out.println("EASY Scores Pressed");
					((AbstractButton) e.getSource()).setBorder(BorderFactory.createLineBorder(Color.white));
					difficulty = Difficulty.Easy;
				}else if(((JButton) e.getSource()).getText().equals("HARD")){
					//System.out.println("HARD Pressed");
					((AbstractButton) e.getSource()).setBorder(BorderFactory.createLineBorder(Color.white));
					difficulty = Difficulty.Medium;
				}else if(((JButton) e.getSource()).getText().equals("BACK")){

					//System.out.println("Exit Pressed");

					frame.getContentPane().removeAll();
					Menu menu = new Menu(frame, notify);
					frame.add(menu);
					frame.getContentPane().validate();
					frame.getContentPane().repaint();
					//call menu jpanel constructor
				}
			}
		});

		return button;
	}







}
