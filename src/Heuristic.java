public interface Heuristic<N> {
	/**
	 * Get an estimate of the distance of the current node
	 * from the goal node, given the previous node to aid the
	 * calculation.
	 *
	 * @param currNode The current node.
	 * @param prevNode The previous node i.e. the parent of the currNode.
	 *
	 * @return Integer representing estimated distance of the
	 * 		   current node from the goal node, given the previous node
	 * 		   to aid the calculation.
	 */
	public int approxDistFromGoal(N currNode, N prevNode);
}
