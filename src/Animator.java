import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.util.ArrayList;

public class Animator implements ActionListener {
	private final static int PIXELS_TO_MOVE = 3; // Pixels to move per loop iteration
	private Coordinate start;
	private Coordinate temp;	// The JLabel's coordinates during the animation
	private Direction direction;
	private int count;	// The number of times the timer has been triggered
	public boolean finished;	// If the animation has finished
	private JPanel glass;
	private JLabel toMove;
	private JPanel root;
	private Timer t;
	private ArrayList<AnimatorNotifier> toCall;
	/**
	 * This will initialise the Animation object but will not start it (call start() )
	 * @param _root The original JPanel that _toMove will be placed back onto
	 * @param _glass The JPanel where the animation will take place
	 * @param _toMove The JLabel which will be moved
	 * @param _start The starting location (in pixel x,y coordinates)
	 * @param d Which direction the animation will go (Can only move one grid space at a time)
	 */
	Animator(JPanel _root, JPanel _glass, JLabel _toMove, Coordinate _start, Direction d) {
		root = _root;
		glass = _glass;
		toMove = _toMove;
		start = _start.clone();
		temp = _start.clone();
		direction = d;
		count = 0;
		t = new Timer(1, this);
		t.setInitialDelay(0);
		toCall = new ArrayList<AnimatorNotifier>();
	}
	public void addCaller(AnimatorNotifier a) {
		toCall.add(a);
	}
	/**
	 * This is run every time the timer is triggered it just moves
	 * the JLabel by PIXELS_PER_POINT in the direction specified
	 * in the constructor
	 */
	public void actionPerformed(ActionEvent e) {
		temp.MoveTimes(direction, PIXELS_TO_MOVE);
		count += PIXELS_TO_MOVE;
		toMove.setBounds(temp.x(),temp.y(), UI.PIXELS_PER_GRID, UI.PIXELS_PER_GRID);

		if(count >= UI.PIXELS_PER_GRID) {
			finish();
		}
		glass.repaint();
	}

	/**
	 * This can either be called seperatley or once the animation
	 * is finished in either case it will remove the JLabel from
	 * the glass pane and place it back in the root
	 */
	public void finish() {
		if(finished == false) {
			finished = true;

			// Remove 'toMove' from glass
			Component toRemove = glass.getComponentAt(temp.x(), temp.y());
			glass.remove(toRemove);

			// If our alignment is off by having PIXELS_PER_MOVE not divide PIXELS_PER_GRID
			// evenly then we re align the image otherwise this has no effect
			temp = start;
			temp.MoveTimes(direction, UI.PIXELS_PER_GRID);

			// Remove object at 'toMove' location on root
			toRemove = root.getComponentAt(temp.x(), temp.y());
			root.remove(toRemove);

			// Create a new panel
			JPanel p = new JPanel(new BorderLayout());
			p.setPreferredSize(new Dimension(UI.PIXELS_PER_GRID, UI.PIXELS_PER_GRID));
			p.setBorder(BorderFactory.createLineBorder(Color.black));
			p.setBackground(Color.WHITE);
			p.add(toMove, BorderLayout.CENTER);

			GridBagConstraints c = new GridBagConstraints();
			c.gridx = temp.x()/UI.PIXELS_PER_GRID;
			c.gridy = temp.y()/UI.PIXELS_PER_GRID;
			root.add(p, c);
			root.revalidate();
			t.stop();
			for(int i = 0; i < toCall.size(); i++) {
				toCall.get(i).finished();
			}
		}
	}

	/**
	 * This starts the animation, it does not remove the JLabel from the
	 * root JPanel
	 */
	public void start() {
		toMove.setBounds(start.x(), start.y(), UI.PIXELS_PER_GRID, UI.PIXELS_PER_GRID);
		glass.add(toMove);
		finished = false;
		t.start();
	}
}
