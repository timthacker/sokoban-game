import java.util.HashSet;
import java.util.PriorityQueue;

public class BoxGoalDistanceHeuristic implements Heuristic<State>{

	/**
	 * Approach for heuristic calculation:
	 * Find Manhattan distance of each box from it closest goal,
	 * sum all of these distances.
	 *
	 * It also gets the distance from the player to the closest box
	 * which isn't in a hole.
	 *
	 * A higher priority is given to nodes which have more boxes
	 * completed than the previous node.
	 *
	 * @return Heuristic value for the current state.
	 */
	@Override
	public int approxDistFromGoal(State currNode, State prevNode) {
		if(prevNode != null) {
			if(currNode.getMap().getRemainingNumBoxes() < prevNode.getMap().getRemainingNumBoxes()) {
				return 0;
			}
		}
		int h = getMovesToBox(currNode.getMap());
		HashSet<Coordinate> boxes = currNode.getMap().getBoxes();
		HashSet<Coordinate> goals = currNode.getMap().getGoals();

		PriorityQueue<Double> distances = new PriorityQueue<Double>();
		for (Coordinate box : boxes){
			distances.clear();
			for (Coordinate goal : goals){
				distances.add(box.MD(goal));
			}
			if (!distances.isEmpty()){
				h += distances.peek();
			}
		}
		return h;
	}

	/**
	 * @return an int containing the number of moves to the closest box not in a hole
	 */
	private int getMovesToBox(Map m) {
		double ret = -1;
		for(Coordinate box : m.getBoxes()) {
			if(m.getGoals().contains(box) == false) { // If the box isn't on a hole
				if(m.getPlayer().getPos().MD(box) < ret || ret == -1) {
					ret = m.getPlayer().getPos().MD(box);
				}
			}
		}
		//return 0; // Uncomment to Disable
		return (int) ret;
	}
}
