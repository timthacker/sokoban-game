import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;

/**
 * An A* search algorithm implementation.
 * The A* is first instantiated with a map.
 * The search method can then be called to search
 * for an optimal solution from the provided map
 * to the goal state, if a solution exists.
 */
public class Astar {
	private int numExpanded;
	private Map world;
	private Heuristic<State> heuristic;
	private boolean solutionFound;
	
	/**
	 * Instantiate a new Astar search.
	 * 
	 * @param world The current map.
	 */
	public Astar(Map world){
		this.world = world;
		this.heuristic = new BoxGoalDistanceHeuristic();
		this.solutionFound = false;
		this.numExpanded = -1;
	}

	/**
	 * Find the optimal solution to the current map, if one exists.
	 * Along with returning the solution, this method will also
	 * set the appropriate search result variables (solutionFound,
	 * and numExpanded).
	 * @return A list of directions from the map provided to the
	 * 			constructor, to the goal state of all boxes in holes.
	 */
	public List<Direction> getSolution(){
		solutionFound = false;
		numExpanded = 0; // used for debugging
		
		// Set of all visited nodes
		HashSet<State> visited = new HashSet<State>();
		
		// Store the best gScore for each discovered state
		HashMap<State, Integer> gScores = new HashMap<State, Integer>();
		
		// For each state, store the most efficient previous state
		HashMap<State, State> cameFrom = new HashMap<State, State>();
		
		/*
		 * Nodes that have been discovered, but are yet to be explored.
		 * 
		 * A priority queue is used for efficient access to lowest-cost node,
		 * and a hash set is used for efficient checking of whether an item
		 * is on the frontier. When updating one of these data structures, make
		 * sure the other one is also updated accordingly.
		 */
		PriorityQueue<State> frontier = new PriorityQueue<State>(new FScoreComparator());
		HashSet<State> frontierSet = new HashSet<State>();
		
		// Generate the initial state
		State currState = new State(world, 0);
		currState.setHScore(heuristic.approxDistFromGoal(currState, null));
		frontier.add(currState);
		frontierSet.add(currState);
		
		while (!frontier.isEmpty()){
			// Remove lowest cost node from frontier
			currState = frontier.poll();
			frontierSet.remove(currState);
			
			// Mark node as visited
			visited.add(currState);
			
			// If node is goal state, return the solution
			if (currState.isGoal()){
				solutionFound = true;
				return reconstruct(cameFrom, currState);
			}
			
			// Generate successors of the current node
			List<State> successors = currState.getChildren();
			numExpanded++;
			for (State successor : successors){
				// Don't explore nodes that have already been seen
				if (visited.contains(successor)) continue;
				
				// Calculate heuristic for successor
				successor.setHScore(heuristic.approxDistFromGoal(successor, currState));

				if (!frontierSet.contains(successor)){
					// Add unexplored nodes to the frontier
					frontier.add(successor);
					frontierSet.add(successor);
				} else if (gScores.containsKey(successor)){
					// If we have seen this node before,
					// discard it if it has a higher gScore
					if (successor.getGScore() >= gScores.get(successor)){
						continue;
					}
				}
				
				cameFrom.put(successor, currState);
				gScores.put(successor, successor.getGScore());
			}
		}
		
		solutionFound = false;
		return null;
	}
	
	/**
	 * Helper function that reconstructs the path to the solution.
	 * 
	 * @param path A map, where each state is mapped to the state that
	 * 				was previous to it.
	 * @param last The state that you want to be last in the reconstructed
	 * 				path.
	 * @return A list of directions that, for all states, have the state previous 
	 * 			to the current state always directly before the current state.
	 */
	private List<Direction> reconstruct(HashMap<State,State> path, State last){
		List<Direction> correct = new ArrayList<Direction>();
		State curr = last;
		Coordinate oldPos = curr.getMap().getPlayer().getPos();
		Coordinate newPos = oldPos;
		while (path.containsKey(curr)){
			oldPos = newPos;
			curr = path.get(curr);
			newPos = curr.getMap().getPlayer().getPos();
			if (newPos.x() > oldPos.x()) correct.add(0, Direction.Left);
			else if (newPos.x() < oldPos.x()) correct.add(0, Direction.Right);
			else if (newPos.y() > oldPos.y()) correct.add(0, Direction.Up);
			else correct.add(0,Direction.Down);
		}
		return correct;
	}
	
	/**
	 * Get a boolean value indicating whether the
	 * most recent search found a solution.
	 * 
	 * @return True if the most recent search found a solution,
	 * 			otherwise false. Also false if no search has
	 * 			been conducted yet.
	 */
	public boolean getSolutionFound(){
		return solutionFound;
	}
	
	/**
	 * Get the number of nodes expanded in the 
	 * previous search.
	 * 
	 * @return Number of nodes expanded in previous search.
	 * 			Returns -1 if no search has been conducted yet.
	 */
	public int getNumExpanded(){
		return numExpanded;
	}
}


