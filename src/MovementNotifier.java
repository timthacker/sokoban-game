public interface MovementNotifier {
	/**
	 * @param d the direction the player moved
	 * @param player the original coordinate of the player before the move
	 * @param replace the tile that the player was on top of
	 */
	public void movePlayer(Direction d, Coordinate player, Tile replace);

	/**
	 * @param d the direction the player moved
	 * @param box the original coordinate of the box before the move
	 * @param replace the tile that the player was on top of
	 * @param onto the tile that the box moved onto
	 */
	public void moveBox(Direction d, Coordinate box, Tile replace, Tile onto);
}
