public enum Difficulty {
	Easy,
	Medium,
	Hard;
	
	/**
	 * Increase the difficulty by 1 unit, if possible.
	 * 
	 * @return A difficulty that is one unit above the current
	 * 		  difficulty. If already at highest difficulty, return
	 * 		  the same difficulty.
	 */
	public Difficulty increaseDifficulty() {
		switch(this) {
			case Easy:
				return Medium;
			case Medium:
				return Hard;
			case Hard:
				return Hard;
		}
		return this;
	}
	
	/**
	 * Decrease the difficulty by 1 unit, if possible.
	 * 
	 * @return A difficulty that is one unit below the current
	 * 		  difficulty. If already at lowest difficulty, return
	 * 		  the same difficulty.
	 */
	public Difficulty decreaseDifficulty(){
		switch(this) {
			case Easy:
				return Easy;
			case Medium:
				return Easy;
			case Hard:
				return Medium;
		}
		return this;
	}
	
	/**
	 * Get the number of boxes in a map for the current difficulty.
	 * 
	 * @return Number of boxes in a map for the current difficulty.
	 */
	public int getNumBoxes() {
		switch(this) {
			case Easy:
				return 2;
			case Medium:
				return 2;
			case Hard:
				return 4;
		}
		
		return 0;
	}
	
	/**
	 * Get the width of a map for the current difficulty.
	 * 
	 * @return Width of the map for the current difficulty.
	 */
	public int getWidth() {
		switch(this) {
			case Easy:
				return 11;
			case Medium:
				return 14;
			case Hard:
				return 17;
		}
		
		return 0;
	}
	
	/**
	 * Get the height of a map for the current difficulty.
	 * 
	 * @return Height of the map for the current difficulty.
	 */
	public int getHeight() {
		switch(this) {
			case Easy:
				return 11;
			case Medium:
				return 14;
			case Hard:
				return 17;
		}
		
		return 0;
	}
}
