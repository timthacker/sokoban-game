public class Player implements Cloneable {
	private String name;
	Coordinate position;
	int score;
	Direction dir;
	
	public Player(String name, Coordinate position){
		this.name = name;
		this.position = position;
		this.score = 0;
		this.dir = Direction.Up;
	}
	
	public void setPos(Coordinate c){
		position = c;
	}
	
	public void setX(int x){
		position.setX(x);
	}
	
	public void setY(int y){
		position.setY(y);
	}
	
	public Coordinate getPos(){
		return position;
	}
	
	public void incScore(int amount){
		score += amount;
	}
	
	public void decScore(int amount){
		score -= amount;
		if (score < 0) score = 0;
	}
	
	public void resetScore(){
		score = 0;
	}
	
	public int getScore(){
		return score;
	}
	
	public String getName(){
		return name;
	}
	
	public void setScore(int score){
		this.score = score;
	}
	
	public void setDir(Direction d){
		dir = d;
	}
	
	public Direction getDir(){
		return dir;
	}
	
	@Override
	public Player clone() {
		Player newPlayer = new Player(this.name, this.position.clone());
		return newPlayer;
	}
}
