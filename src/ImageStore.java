import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.ImageIcon;
public class ImageStore {
	private ImageIcon img;
	private boolean loaded;

	/**
	 * @param path the file name for the image (can be relative to .jar file)
	 * @param fallbackColour the colour which will be show if the image can't be found
	 * 		the integer is specified in the ARGB format
	 * @post If the image was found and loaded the loaded variable will be set to true
	 */
	ImageStore(String path, int fallbackColour) {
		File check = new File(path);
		if(check.exists()) {
			ImageIcon toScale = new ImageIcon(path);
			img = new ImageIcon(toScale.getImage().getScaledInstance(UI.PIXELS_PER_GRID, UI.PIXELS_PER_GRID, Image.SCALE_SMOOTH));
			loaded = true;
		} else {
			BufferedImage fallback = new BufferedImage(UI.PIXELS_PER_GRID, UI.PIXELS_PER_GRID, BufferedImage.TYPE_INT_ARGB);
			for(int x = 0; x <  UI.PIXELS_PER_GRID; x++) {
				for(int y = 0; y < UI.PIXELS_PER_GRID; y++) {
					fallback.setRGB(x,y,fallbackColour);
				}
			}
			img = new ImageIcon(fallback);
			//System.out.println("Image: " + path + " doesn't exist");
		}
	}
	ImageStore(Image _img) {
		loaded = true;
		img = new ImageIcon(_img);
	}
	public boolean isLoaded() {
		return loaded;
	}

	/**
	 * @return Will return the ImageIcon if it was loaded else it will return null
	 */
	public ImageIcon getImage() {
		return img;
	}
}
