public enum Direction {
	Up,
	Down,
	Left,
	Right;
	
	private Direction opposite;
	
	static {
		Up.opposite = Down;
		Down.opposite = Up;
		Left.opposite = Right;
		Right.opposite = Left;
	}
	
	/**
	 * Get the opposite direction to the current direction.
	 * 
	 * @return Opposite direction to current direction.
	 */
	public Direction getOpposite() {
		return opposite;
	}
	
	/**
	 * Convert each direction to a string
	 */
	public String toString() {
		switch(this) {
			case Up: return "U";
			case Down: return "D";
			case Left: return "L";
			case Right: return "R";
		default:
			break;
		}
		return "";
	}
}
