import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.HashSet;
public class MapGenerator {
	private Map            map;
	private int            width;
	private int            height;
	private int            numBoxes;
	private List<Template> templates;
	static int numGenerated = 0;


	// 3 x 3 Templates

	private Tile template1[][] = {
			{Tile.Wall,  Tile.Wall,  Tile.Wall},
			{Tile.Empty, Tile.Empty, Tile.Empty},
			{Tile.Empty, Tile.Empty, Tile.Empty}
	};
	private int templateWidth1 = 3;
	private int templateHeight1 = 3;

	private Tile template2[][] = {
			{Tile.Empty, Tile.Wall, Tile.Empty},
			{Tile.Empty, Tile.Wall,  Tile.Empty},
			{Tile.Empty, Tile.Wall, Tile.Empty}
	};
	private int templateWidth2 = 3;
	private int templateHeight2 = 3;

	private Tile template3[][] = {
			{Tile.Wall, Tile.Empty, Tile.Empty},
			{Tile.Empty, Tile.Wall, Tile.Empty},
			{Tile.Empty, Tile.Empty, Tile.Empty}
	};
	private int templateWidth3 = 3;
	private int templateHeight3 = 3;

	private Tile template4[][] = {
			{Tile.Wall, Tile.Empty, Tile.Empty},
			{Tile.Wall, Tile.Empty, Tile.Empty},
			{Tile.Wall, Tile.Wall, Tile.Empty}
	};
	private int templateWidth4 = 3;
	private int templateHeight4 = 3;

	private Tile template5[][] = {
			{Tile.Wall, Tile.Empty, Tile.Empty},
			{Tile.Wall, Tile.Wall, Tile.Empty},
			{Tile.Wall, Tile.Empty, Tile.Empty}
	};
	private int templateWidth5 = 3;
	private int templateHeight5 = 3;

	private Tile template6[][] = {
			{Tile.Wall, Tile.Empty, Tile.Empty},
			{Tile.Wall, Tile.Wall, Tile.Wall},
			{Tile.Empty, Tile.Empty, Tile.Empty}
	};
	private int templateWidth6 = 3;
	private int templateHeight6 = 3;

	private Tile template7[][] = {
			{Tile.Empty, Tile.Empty, Tile.Wall},
			{Tile.Empty, Tile.Empty, Tile.Wall},
			{Tile.Empty, Tile.Empty, Tile.Wall}
	};
	private int templateWidth7 = 3;
	private int templateHeight7 = 3;

	private Tile template8[][] = {
			{Tile.Empty, Tile.Empty, Tile.Wall},
			{Tile.Empty, Tile.Wall, Tile.Wall},
			{Tile.Empty, Tile.Empty, Tile.Wall}
	};
	private int templateWidth8 = 3;
	private int templateHeight8 = 3;

	/**
	 * Constructor for MapGenerator class
	 * Generates a random map
	 * Sets the map dimensions and number of boxes
	 * Creates list of templates to create the map randomly
	 *
	 * @param map Map object to generate a map for
	 */
	public MapGenerator(Map map) {
		numGenerated++;
		this.map      = map;
		this.width    = map.getWidth();
		this.height   = map.getHeight();
		this.numBoxes = map.getInitialNumBoxes();

		Template template1 = new Template(templateWidth1, templateHeight1, this.template1);
		Template template2 = new Template(templateWidth2, templateHeight2, this.template2);
		Template template3 = new Template(templateWidth3, templateHeight3, this.template3);
		Template template4 = new Template(templateWidth4, templateHeight4, this.template4);
		Template template5 = new Template(templateWidth5, templateHeight5, this.template5);
		Template template6 = new Template(templateWidth6, templateHeight6, this.template6);
		Template template7 = new Template(templateWidth7, templateHeight7, this.template7);
		Template template8 = new Template(templateWidth8, templateHeight8, this.template8);

		templates = new ArrayList<Template>();

		templates.add(template1);
		templates.add(template2);
		templates.add(template3);
		templates.add(template4);
		templates.add(template5);
		templates.add(template6);
		templates.add(template7);
		templates.add(template8);
	}

	/**
	 * Generates a new random solvable map grid
	 */
	public void getNewMapGrid() {
		boolean isValidMap = false;

		while (!isValidMap) {
			addPrototype();
			addTemplates();

			if (getNumEmptyBoxTiles() < numBoxes * 2 || !isInteresting()) continue;

			addBoxesAndGoals();
			map.setPlayer(genPlayer("Player 1"));

			if (isSolvable()) {
				map.findSolution();
				if (map.getSol() != null){
					if (map.getSol().size() >= 27){
						isValidMap = true;
						numGenerated++;
					}
				}
			}
		}

	}

	/**
	 * Generates walls around the perimeter.
	 */
	private void addPrototype() {
		Tile grid[][] = map.getGrid();

		for(int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++){
				if (y == 0 || y == height-1 || x == 0 || x == width-1){
					grid[y][x] = Tile.Wall;
				} else {
					grid[y][x] = Tile.Empty;
				}
			}
		}
	}

	/**
	 * Adds the templates randomly to the map
	 */
	private void addTemplates() {
		// Place templates randomly overlapping the prototype
		Coordinate c = new Coordinate(1, 1);
		int limit = ((width - 2) / 3) * ((width - 2) / 3);

		for (int i = 0; i < limit; i++) {

			Random r = new Random();
			int index = r.nextInt(8);
			Template template = templates.get(index);
			addSubGrid(template.getGrid(), c.x(), c.y(), template.getWidth(), template.getHeight());
			if (c.x() == width - 4) {
				c.setX(1);
				c.setY(c.y() + 3);
			} else {
				c.setX(c.x() + 3);
			}
		}

	}

	/**
	 * Adds a sub section of a grid to the map grid
	 *
	 * @param subGrid Sub section array
	 * @param x Coordinate of the top left corner in the map grid to place the sub grid
	 * @param y Coordinate of the top left corner in the map grid to place the sub grid
	 * @param gridWidth Width of the sub grid
	 * @param gridHeight Height of the sub grid
	 */
	private void addSubGrid(Tile subGrid[][], int x, int y, int gridWidth, int gridHeight) {
		Tile grid[][] = map.getGrid();

		int subGridX = 0;
		int subGridY = 0;

		loop:
		for (int yGrid = 1; yGrid < height - 1; yGrid++) {
			for (int xGrid = 1; xGrid < width - 1; xGrid++) {
				if ((yGrid == y + subGridY) && (xGrid == x + subGridX)) {
					grid[yGrid][xGrid] = subGrid[subGridY][subGridX];
					if (subGridX == gridWidth - 1) {
						if (subGridY == gridHeight - 1) break loop;
						subGridX = 0;
						subGridY++;
					} else {
						subGridX++;
					}
				}
			}
		}
	}

	/**
	 * Add up to 'numBoxes' boxes to the map.
	 * Less may be added if the map is not solvable
	 * with the number of desired boxes.
	 *
	 * Note that this method also resets the map's store of
	 * box and goal positions, as well as the number of boxes
	 * remaining.
	 *
	 * @param numBoxes Number of boxes/goals to add to the map.
	 */
	private void addBoxesAndGoals() {
		map.getBoxes().clear();
		map.getGoals().clear();
		map.setRemainingNumBoxes(0);

		for (int boxNum = 0; boxNum < numBoxes; boxNum++){
			Coordinate box = getRandomEmptyTile("box");
			map.addBox(box);
		}

		for (int boxNum = 0; boxNum < numBoxes; boxNum++){
			Coordinate goal = getRandomEmptyTile("goal");
			map.addGoal(goal);
		}
	}

	/**
	 * Gets the number of empty tiles a box is able to placed in
	 * @return total Number of tiles
	 */
	private int getNumEmptyBoxTiles(){
		Tile grid[][] = map.getGrid();

		int total = 0;
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				if (grid[y][x] == Tile.Empty){
					if (!isAdjacentWall(x, y)) total++;
				}
			}
		}
		return total;
	}

	/**
	 * Get the coordinate for a random empty tile in the map.
	 * @return Coordinate of a random empty tile in the map.
	 * 			If no tiles are empty, null is returned.
	 */
	private Coordinate getRandomEmptyTile(String type){
		Tile grid[][] = map.getGrid();

		// Store all empty tile coordinates in a list
		List<Coordinate> emptyTiles = new ArrayList<Coordinate>();
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				if (grid[y][x] == Tile.Empty){
					if (Objects.equals(type, "box")) {
						int numAdjacentWalls = 0;

						if (Objects.equals(grid[y + 1][x], Tile.Wall)) {
							numAdjacentWalls++;
						} else if (Objects.equals(grid[y][x + 1], Tile.Wall)) {
							numAdjacentWalls++;
						} else if (Objects.equals(grid[y - 1][x], Tile.Wall)) {
							numAdjacentWalls++;
						} else if (Objects.equals(grid[y][x - 1], Tile.Wall)) {
							numAdjacentWalls++;
						}

						if (numAdjacentWalls == 0) {
							emptyTiles.add(new Coordinate(x, y));
						}
					} else if (Objects.equals(type, "goal")) {
						emptyTiles.add(new Coordinate(x, y));
					} else if (Objects.equals(type, "player")) {
						emptyTiles.add(new Coordinate(x, y));
					}
				}
			}
		}

		// Check if there is at least 1 empty square
		Random r = new Random();
		if (emptyTiles.size() != 0){
			int min = 0;
			int max = emptyTiles.size()-1;
			return emptyTiles.get(r.nextInt((max - min) + 1) + min);
		} else{
			return null;
		}
	}

	/**
	 * Returns true if there is a wall adjacent to the the given coordinate
	 *
	 * @param x X coordinate.
	 * @param y Y coordinate.
	 * @return isAdjacentWall True is wall is adjacent, false otherwise.
	 */
	private boolean isAdjacentWall(int x, int y) {
		Tile grid[][] = map.getGrid();

		boolean isAdjacentWall = false;

		if (Objects.equals(grid[y + 1][x], Tile.Wall)) {
			isAdjacentWall = true;
		} else if (Objects.equals(grid[y][x + 1], Tile.Wall)) {
			isAdjacentWall = true;
		} else if (Objects.equals(grid[y - 1][x], Tile.Wall)) {
			isAdjacentWall = true;
		} else if (Objects.equals(grid[y][x - 1], Tile.Wall)) {
			isAdjacentWall = true;
		}

		return isAdjacentWall;
	}

	/**
	 * Place a player on the map, and return the player
	 * object.
	 *
	 * @param name Name of the player.
	 * @return The player that was created.
	 */
	private Player genPlayer(String name){
		Tile grid[][] = map.getGrid();

		Player p = new Player(name, getRandomEmptyTile("player"));
		// Place the player in a random empty square
		Coordinate playerPos = p.getPos();
		grid[playerPos.y()][playerPos.x()] = Tile.Player;

		return p;
	}

	/**
	 * Returns true if the map is solvable
	 * It works by creating a bitmap array the same size as the map
	 * and checking to see where the player and the boxes can be pushed
	 * if each box can be pushed into it's own hole it returns true
	 * @return isSolvable
	 */
	private boolean isSolvable() {
		int test[][] = new int[map.getHeight()][map.getWidth()];
		int num = 0x01;
		clearPlayer(test);
		fillPlayer(test, map.getPlayer().getPos().clone(), null);
		for(Coordinate b : map.getBoxes()) {
			fillBoxes(test, b.clone(), num);
			num = num << 1;
		}
		HashMap<Coordinate, Integer> goals = new HashMap<Coordinate, Integer>();
		int boxMatch = 0;
		for(Coordinate h : map.getGoals()) {
			if( (test[h.y()][h.x()] & ((1 << numBoxes) -1))  == 0) { // A a goal doesn't have a box
				return false;
			}
			boxMatch |= test[h.y()][h.x()] & ((1<<numBoxes)-1);
			goals.put(h, test[h.y()][h.x()]);
		}
		if(boxMatch != ((1<<numBoxes)-1)) {
			return false;
		}
		if(checkUnique(goals) == false) {
			return false;
		}
		return true;
	}

	/**
	 * Check that the goal map is unique.
	 * By checking the bitmask of each goal assign priorities based on
	 * how many of the boxes can reach each goal. Then the box with the
	 * highest priority is 'placed' into a box and the rest are forbidden
	 * from acessing the box. If none of the bitmasks end up being zero
	 * then there is a unique mapping
	 * @param goalMap Goal map to check.
	 * @return True if unique, false otherwise.
	 */
	private boolean checkUnique(HashMap<Coordinate, Integer> goalMap) {
		HashMap<Integer, Integer> priorities = new HashMap<Integer, Integer>();
		for(int i = 0x01; i < (1 << numBoxes); i = i << 1) {
			Integer count = 0;
			for(Coordinate g : goalMap.keySet()) {
				if( (goalMap.get(g) & i) == i ) {
					count++;
				}
			}
			priorities.put(i, count);
		}
		Set<Coordinate> visited = new HashSet<Coordinate>();
		for(Coordinate g : goalMap.keySet()) {

			if( (goalMap.get(g) & ( (1<<numBoxes) - 1)) == 0) {
				return false;
			}
			int max = -1;
			for(int i = 0x01; i < (1 << numBoxes); i = i << 1) {
				if( (max == -1 || ( priorities.get(i) > priorities.get(max) ) ) && (goalMap.get(g) & i) == i) {
					max = i; // Find the maximum priority for the particular bit
				}
			}
			// Now that we have found the max we must remove it from the other goals
			visited.add(g);
			for(Coordinate modify : goalMap.keySet()) {
				if(visited.contains(modify) == false) {
					goalMap.put(modify, goalMap.get(modify) & (~max));
				}
			}
		}
		return true;
	}

	/**
	 * Recursivley fill the test array with all the places the player can go
	 */
	private void fillPlayer(int[][] test, Coordinate original, Coordinate placeBox) {
		test[original.y()][original.x()] |= 0x80;
		for(Direction d : Direction.values()) {
			Coordinate player = original.clone();
			player.Move(d);
			if(player.y() < 0 || player.y() >= map.getHeight() || player.x() < 0 || player.x() >= map.getWidth()) {
				continue;
			}
			if( (test[player.y()][player.x()] & 0x80) == 0x80) {
				continue; // If the player has been there before
			}
			if(player.equals(placeBox)) {
				continue;
			}
			switch(map.getTile(player.x(), player.y())) {
			case Hole:
			case Empty:
			case Box:
				test[player.y()][player.x()] |= 0x80;
				fillPlayer(test, player, placeBox);
				break;
			default:
				break;
			}
		}
	}
	/**
	 * Recursivley fill the test array with all the places a box can go,
	 * speical consideration must be take for if the player blocks of the
	 * only path to a hole, if this is the only way forbid it from going ther
	 */
	private void fillBoxes(int[][] test, Coordinate original, int num) {
		int[][] temp = new int[map.getHeight()][map.getWidth()];

		for(Direction d : Direction.values()) {
			Coordinate box = original.clone();
			box.Move(d.getOpposite());
			if( (test[box.y()][box.x()] & 0x80) != 0x80) {
				continue; // The Box cannot be pushed from this direction
			}
			clearPlayer(temp);
			fillPlayer(temp, box, original);
			box = original.clone();
			box.Move(d);
			if( (temp[box.y()][box.x()] & 0x80) != 0x80) {
				continue; // The Box cannot be pushed from this direction
			}
			if( (test[box.y()][box.x()] & num) == num) {
				continue; // The Box has been here before
			}
			switch(map.getTile(box.x(), box.y())) {
				case Empty:
				case Hole:
				case Box:
				case Player:
					test[original.y()][original.x()] |= 0x80;
					test[box.y()][box.x()] |= num;
					fillBoxes(test, box, num);
					break;
				default:
					break;
			}
		}
	}
	/**
	 * Remove the player bit from the given test array
	 */
	private void clearPlayer(int[][]test) {
		for(int y = 0; y < map.getHeight(); y++) {
			for(int x = 0; x < map.getWidth(); x++) {
				test[y][x] = test[y][x] & (~0x80);
			}
		}
	}

	/**
	 * Returns true if the map is interesting
	 * Checks that there isn't any invalid tiles
	 *
	 * @return True is map is 'interesting', false otherwise
	 */
	private boolean isInteresting() {
		Tile grid[][] = map.getGrid();

		int numAdjacentWalls = 0;
		for(int y = 1; y < height - 1; y++) {
			for(int x = 1; x < width - 1; x++) {
				if (Objects.equals(grid[y][x], Tile.Empty)) {
					if (Objects.equals(grid[y + 1][x], Tile.Wall)) {
						numAdjacentWalls++;
					} if (Objects.equals(grid[y][x + 1], Tile.Wall)) {
						numAdjacentWalls++;
					} if (Objects.equals(grid[y - 1][x], Tile.Wall)) {
						numAdjacentWalls++;
					} if (Objects.equals(grid[y][x - 1], Tile.Wall)) {
						numAdjacentWalls++;
					}

					if (numAdjacentWalls >= 3) return false;
					numAdjacentWalls = 0;
				}
			}
		}

		return true;
	}
}
