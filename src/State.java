import java.util.ArrayList;
import java.util.List;

public class State {
	private Map world;
	private int hScore;
	private int gScore;
	private static final int INFINITY = -1;

	public State(Map world, int gScore){
		this.world = world;
		this.gScore = gScore;
		this.hScore = INFINITY;
	}
	
	public boolean isGoal(){
		if (world.getRemainingNumBoxes() == 0) return true;
		else return false;
	}
	
	public List<State> getChildren(){
		List<State> children = new ArrayList<State>();
		
		List<Direction> possibleMoves = world.getPossibleMoves();
		for (Direction move : possibleMoves){
			Map child = world.clone();
			child.movePlayer(move);
			children.add(new State(child, gScore+1));
		}
		
		return children;
	}
	
	public void setHScore(int hScore){
		this.hScore = hScore;
	}
	
	public int getHScore(){
		return hScore;
	}
	
	public void setGScore(int gScore){
		this.gScore = gScore;
	}
	
	public int getGScore(){
		return gScore;
	}
	
	public Map getMap(){
		return world;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((world == null) ? 0 : world.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		State other = (State) obj;
		if (world == null) {
			if (other.world != null) return false;
		} else if (!world.equals(other.world)) return false;
		return true;
	}


}
