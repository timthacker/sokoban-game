
public class Template {
	private int width;
	private int height;
	private Tile grid[][];
	
	public Template(int width, int height, Tile grid[][]) {
		this.setWidth(width);
		this.setHeight(height);
		this.setGrid(grid);
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Tile[][] getGrid() {
		return grid;
	}

	public void setGrid(Tile grid[][]) {
		this.grid = grid;
	}
}
