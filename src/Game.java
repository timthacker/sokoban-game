/**
 * BUGS:
 * 	- Pressing undo fast can sometimes cause game to freeze
 */

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Game implements KeyListener, UiNotifier, StateNotifier {

	private Stack<Map> prevMoves;
	private Map map;
	private UI ui;
	private Difficulty difficulty;
	private MapStore store;
	private Music music;
	private static final int MAX_NUM_SCORES = 3;

	Game() {
		prevMoves = new Stack<Map>();

		ui = new UI(this, this);
		ui.addCaller(this);

		music = new Music();
	}

	public static void main(String[] args) {
		Game g = new Game();
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		Player currPlayer = map.getPlayer();

		if (e.getKeyChar() == 'u' && !prevMoves.isEmpty()) {
			// Undo function
			int oldScore = currPlayer.getScore();
			map = prevMoves.pop();
			map.addMovementNotifier(ui);
			ui.reDrawUi(map);
			map.getPlayer().setScore(oldScore+1);
			return;
		}
		Map prev = map.clone();

		int code = e.getKeyCode();
		if (code == KeyEvent.VK_UP || code == KeyEvent.VK_W){
			if(map.movePlayer(Direction.Up)) {
				prevMoves.push(prev);
				currPlayer.incScore(1);
			}
		} else if (code == KeyEvent.VK_DOWN || code == KeyEvent.VK_S){
			if(map.movePlayer(Direction.Down)) {
				prevMoves.push(prev);
				currPlayer.incScore(1);
			}
		} else if (code == KeyEvent.VK_LEFT || code == KeyEvent.VK_A){
			if(map.movePlayer(Direction.Left)) {
				prevMoves.push(prev);
				currPlayer.incScore(1);
			}
		} else if (code == KeyEvent.VK_RIGHT || code == KeyEvent.VK_D){
			if(map.movePlayer(Direction.Right)) {
				prevMoves.push(prev);
				currPlayer.incScore(1);
			}
		}

		switch(code) {
			case KeyEvent.VK_R:
				resetMap();
				break;
			case KeyEvent.VK_O: // Key: 'o'
				ui.reDrawUi(map);
				break;
			case KeyEvent.VK_ESCAPE:
				if(store != null) {
					store.cancel();
				}
				ui.getWindow().dispatchEvent(new WindowEvent(ui.getWindow(), WindowEvent.WINDOW_CLOSING));
				break;
			default:
				return;
		}
	}
	public void resetMap() {
		map = store.getNext();
		map.addMovementNotifier(ui);
		prevMoves.clear();
		ui.reDrawUi(map);
	}

	public void allAnimationsCompleted() {
		if (map.getRemainingNumBoxes() == 0) {
			music.playSound("finish");

			// Calculate player's performance
			Player p = map.getPlayer();
			int pScore = 100-100*(p.getScore()-map.getSol().size())/p.getScore();

			// Set path
			String fileName = "src/data_store/scores_" + difficulty.toString() + ".csv";
			Path scoresFile = Paths.get(fileName);

			// Get all records from file
			List<String> allRecords = new ArrayList<String>();
			if (Files.exists(scoresFile)){
				try {allRecords = Files.readAllLines(scoresFile);}
				catch (IOException e1){ e1.printStackTrace();}
			}

			// Update records
			String newRecord = p.getName() + "," + pScore;
			for (int lineNum = 0; lineNum < allRecords.size(); lineNum++){
				String record = allRecords.get(lineNum);
				List<String> fields = Arrays.asList(record.split(","));
				int score = Integer.parseInt(fields.get(1));
				if (pScore > score){
					allRecords.add(lineNum, newRecord);
					break;
				}
			}

			// Make sure number of records doesn't exceed maximum allowed
			if (allRecords.size() > MAX_NUM_SCORES && allRecords.size() >= 1){
				// remove last line
				allRecords.remove(allRecords.size() - 1);
			} else if (allRecords.size() == 0){
				// Handle the case where the file was just created
				allRecords.add(newRecord);
			}

			// Write the new records to the file
			try {
				Files.write(scoresFile, allRecords);
			} catch (IOException e) {
				e.printStackTrace();
			}

			resetMap();
		}
	}

	public void stateChanged(Difficulty state) {
		difficulty = state;
		store = new MapStore(state);
		store.start();
		resetMap();
	}
}
