import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;



public class Menu extends JPanel {
	StateNotifier notify;
	Menu(JFrame frame, StateNotifier s){
		super();
		notify = s;
		//Title
		JLabel label = new JLabel("COMP2911 Project");

		label.setFont(new Font("Sans Serif", Font.PLAIN, 30));
		label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		this.add(label);
		this.setOpaque(true);
		//Spaces the Text
		this.add(Box.createRigidArea(new Dimension(0,20)));
		//Adds Components to label
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		addComponents(this, frame);
	}

	private JButton addAButton(String text, JPanel panel, JFrame frame) {
		JButton button = new JButton(text);
		//Menu buttons are invisible
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);

		//Button initialization
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		Dimension maxSize = new Dimension(400, 30);
		button.setMaximumSize(maxSize);
		button.setForeground(Color.black);
		button.setFont(new Font("Sans Serif", Font.BOLD, 30));

		//Action listener for buttons
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				//Events Manager For Menu UI
				if(((JButton) e.getSource()).getText().equals("New Game")){
					frame.getContentPane().removeAll();
					GameSettings g = new GameSettings(frame, notify);
					frame.add(g);
					frame.getContentPane().validate();
					frame.getContentPane().repaint();
				}else if(((JButton) e.getSource()).getText().equals("High Scores")){
					//Add High Scores
				}else if(((JButton) e.getSource()).getText().equals("Tutorial")){
					//Add Tutorial
				}else if(((JButton) e.getSource()).getText().equals("Settings")){
					//Add Settings
				}else if(((JButton) e.getSource()).getText().equals("Exit")){
					//completed
					frame.dispose();
					System.exit(0);
				}
			}
		});
		return button;
	}
	private void addComponents(JPanel panel, JFrame frame){
		this.add(addAButton("New Game", this, frame));
		this.add(Box.createRigidArea(new Dimension(0,30)));
		this.add(addAButton("High Scores", this, frame));
		this.add(Box.createRigidArea(new Dimension(0,30)));
		this.add(addAButton("Tutorial", this, frame));
		this.add(Box.createRigidArea(new Dimension(0,30)));
		this.add(addAButton("Settings", this, frame));
		this.add(Box.createRigidArea(new Dimension(0,30)));
		this.add(addAButton("Exit", this, frame));
	}



}






