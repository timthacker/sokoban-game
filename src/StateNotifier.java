public interface StateNotifier {
	public void stateChanged(Difficulty state);
}
