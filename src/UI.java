import java.util.ArrayList;
import java.awt.event.KeyListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class UI implements MovementNotifier, AnimatorNotifier {

	public final static int PIXELS_PER_GRID = 50;
	private ImageStore playerImage;
	private ImageStore wallImage;
	private ImageStore boxImage;
	private ImageStore floorImage;
	private ImageStore holeImage;
	private ImageStore boxInHoleImage;
	private ImageStore playerOnHoleImage;
	private JFrame window;
	private JPanel main;
	private JPanel glassPane;
	private Animator playerAnimation;
	private Animator boxAnimation;
	private ArrayList<UiNotifier> toCall;
	private Menu menu;
	private JLabel playerTile;
	private KeyListener keyListener;
	private boolean displayingMap;

	public UI(StateNotifier s, KeyListener k) {
		window = new JFrame();
		window.setTitle("2911 Project");
		window.setLocationRelativeTo(null);
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		keyListener = k;
		displayingMap = false;
		toCall = new ArrayList<UiNotifier>();
		main = new JPanel(new GridBagLayout()); // The Main Grid
		glassPane = new JPanel(null);
		glassPane.setOpaque(false);
		playerImage = new ImageStore("src/sprites/Classic/Player.png", 0xFFFF0000);
		wallImage = new ImageStore("src/sprites/Classic/Wall.png", 0xFF0000FF);
		boxImage = new ImageStore("src/sprites/Classic/Box.png", 0xFFDEB887);
		floorImage = new ImageStore("src/sprites/Classic/Floor.png", 0xFFFFFFFF);
		holeImage = new ImageStore("src/sprites/Classic/Hole.png", 0xFF000000);
		boxInHoleImage = new ImageStore("src/sprites/Classic/BoxInHole.png", 0xFF00FF00);
		playerOnHoleImage = new ImageStore("src/sprites/Classic/PlayerOnHole.png", 0xFF00FB14);
		playerAnimation = null;
		boxAnimation = null;

		playerTile = new JLabel(playerImage.getImage());
		menu = new Menu(window, s);
		window.setGlassPane(glassPane);
		menu.revalidate();
		window.add(menu);
		window.pack();
		window.setVisible(true);
	}

	/**
	 * Redraws the UI based off of the map given
	 * @param m the map that the ui should be based upon
	 */
	public void reDrawUi(Map m) {
		window.getContentPane().removeAll();
		glassPane.removeAll();
		main.removeAll(); // Remove all the elements in the current grid
		GridBagConstraints c = new GridBagConstraints();

		for(int y = 0; y < m.getHeight(); y++) {
			for(int x = 0; x < m.getWidth(); x++) {
				JPanel p = new JPanel(new BorderLayout());
				p.setPreferredSize(new Dimension(PIXELS_PER_GRID, PIXELS_PER_GRID));
				p.setBorder(BorderFactory.createLineBorder(Color.BLACK));
				p.setBackground(Color.WHITE);
				switch(m.getTile(x,y)) {
					case Box:
						p.add(new JLabel(boxImage.getImage()), BorderLayout.CENTER);
						break;
					case Wall:
						p.add(new JLabel(wallImage.getImage()), BorderLayout.CENTER);
						break;
					case Empty:
						p.add(new JLabel(floorImage.getImage()), BorderLayout.CENTER);
						break;
					case Player:
						p.add(new JLabel(playerImage.getImage()), BorderLayout.CENTER);
						break;
					case Hole:
						p.add(new JLabel(holeImage.getImage()), BorderLayout.CENTER);
						break;
					case BoxInHole:
						p.add(new JLabel(boxInHoleImage.getImage()), BorderLayout.CENTER);
						break;
					case PlayerOnHole:
						p.add(new JLabel(playerOnHoleImage.getImage()), BorderLayout.CENTER);
						break;
				}
				c.gridx = x;
				c.gridy = y;
				main.add(p, c);
			}

			// place the player in its starting position, on top of the JLabel grid
			playerTile.setBounds(m.getPlayer().getPos().x()*PIXELS_PER_GRID, m.getPlayer().getPos().y()*PIXELS_PER_GRID, UI.PIXELS_PER_GRID, UI.PIXELS_PER_GRID);
			glassPane.setVisible(true);
		}
		window.add(main);
		window.revalidate();
		window.pack(); // This resizes the frame so everything is the correct size
		main.requestFocus();
		if(displayingMap == false) {
			main.addKeyListener(keyListener);
			displayingMap = true;
		}
	}

	/**
	 * Animates the moving of the player (does not update the map representation)
	 * @pre The player is allowed to move in the direction
	 * @param d the direction you want the player on the screen to move
	 * @param player the current coordinates of the player you want to move
	 * @param replace What tile the player's original position will be replaced with (e.g. hole, empty, etc.)
	 */
	public void movePlayer(Direction d, Coordinate player, Tile replace) {
		if(playerAnimation != null) {
			playerAnimation.finish();
			window.revalidate();
		}
		GridBagConstraints c = new GridBagConstraints();
		JPanel p = new JPanel(new BorderLayout());
		p.setPreferredSize(new Dimension(PIXELS_PER_GRID, PIXELS_PER_GRID));
		p.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		// First Remove Player
		Component toRemove = main.getComponentAt(player.x() * PIXELS_PER_GRID, player.y() * PIXELS_PER_GRID);
		main.remove(toRemove);

		c.gridx = player.x();
		c.gridy = player.y();
		switch(replace) {
		case Empty:
			p.add(new JLabel(floorImage.getImage()), BorderLayout.CENTER);
			break;
		case Hole:
			p.add(new JLabel(holeImage.getImage()), BorderLayout.CENTER);
			break;
		case PlayerOnHole:
			p.add(new JLabel(playerOnHoleImage.getImage()), BorderLayout.CENTER);
		default:
			break;
		}
		main.add(p,c);

		playerAnimation = new Animator(main, glassPane, playerTile, new Coordinate(player.x() * PIXELS_PER_GRID, player.y() * PIXELS_PER_GRID), d);
		playerAnimation.addCaller(this);
		playerAnimation.start();

		glassPane.setVisible(true);
		window.pack();
	}

	/**
	 * Animates the moving of the box (does not update the map representation)
	 * @pre The box can move in the specified direction
	 * @param d the direction you want the player on the screen to move
	 * @param box the coordinates of the box you want to move
	 * @param replace the tile that the box's original position will be replaced with (player, playerOnHole, etc.)
	 * @param onto the tile the box will become after the move (either a plane box or a box in hole)
	 */
	public void moveBox(Direction d, Coordinate box, Tile replace, Tile onto) {
		if(boxAnimation != null) {
			boxAnimation.finish();
			window.revalidate();
		}
		GridBagConstraints c = new GridBagConstraints();
		JPanel p = new JPanel(new BorderLayout());
		p.setPreferredSize(new Dimension(PIXELS_PER_GRID, PIXELS_PER_GRID));
		p.setBorder(BorderFactory.createLineBorder(Color.black));

		// First Remove Player
		Component toRemove = main.getComponentAt(box.x() * PIXELS_PER_GRID, box.y() * PIXELS_PER_GRID);
		main.remove(toRemove);

		c.gridx = box.x();
		c.gridy = box.y();
		switch(replace) {
		case Empty:
			p.add(new JLabel(floorImage.getImage()), BorderLayout.CENTER);
			break;
		case Hole:
			p.add(new JLabel(holeImage.getImage()), BorderLayout.CENTER);
			break;
		default:
			break;
		}
		main.add(p,c);

		switch(onto) {
		case Box:
			boxAnimation = new Animator(main, glassPane, new JLabel(boxImage.getImage()), new Coordinate(box.x() * PIXELS_PER_GRID, box.y() * PIXELS_PER_GRID), d);
			break;
		case BoxInHole:
			boxAnimation = new Animator(main, glassPane, new JLabel(boxInHoleImage.getImage()), new Coordinate(box.x() * PIXELS_PER_GRID, box.y() * PIXELS_PER_GRID), d);
			break;
		default:
			break;
		}
		if(boxAnimation != null) {
			boxAnimation.addCaller(this);
			boxAnimation.start();
		}
		glassPane.setVisible(true);
		window.pack();
	}
	public void finished() {
		boolean boxFinished = false;
		boolean playerFinished = false;
		if(boxAnimation != null) {
			if(boxAnimation.finished) {
				boxFinished = true;
			}
		} else {
			boxFinished = true;
		}
		if(playerAnimation != null) {
			if(playerAnimation.finished) {
				playerFinished = true;
			}
		}
		if(boxFinished && playerFinished) {
			for(int i = 0; i < toCall.size(); i++) {
				toCall.get(i).allAnimationsCompleted();
			}
		}
	}
	public void addCaller(UiNotifier a) {
		toCall.add(a);
	}
	public JFrame getWindow() {
		return window;
	}
}
