/**
 * Class for creating an x,y coordinate.
 *
 * Coordinate system is also addressed with (0,0)
 * at the top left, and values increasing toward the
 * bottom right.
 */
public class Coordinate implements Cloneable {
	private int x;
	private int y;
	
	/**
	 * Construct a new coordinate.
	 * 
	 * @param x x value of coordinate.
	 * @param y y value of coordinate.
	 */
	public Coordinate(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Get the x value of the coordinate.
	 * 
	 * @return x value of the coordinate.
	 */
	public int x(){
		return x;
	}
	
	/**
	 * Get the y value of the coordinate.
	 * 
	 * @return y value of the coordinate.
	 */
	public int y(){
		return y;
	}
	
	/**
	 * Move a coordinate in a certain direction by 1 unit.
	 * 
	 * @param c Coordinate to move.
	 * @param d Direction to move in.
	 * @return A new coordinate moved in the specified direction by 1 unit.
	 */
	public void Move (Direction d){
		MoveTimes(d, 1);
	}
	
	/**
	 * Move 'n' number of times in direction 'd'.
	 * 
	 * @param d Direction to move in.
	 * @param n Number of times to move in direction.
	 */
	public void MoveTimes (Direction d, int n){
		switch(d) {
			case Up :
				y -= n;
				break;
			case Down:
				y += n;
				break;
			case Left:
				x -= n;
				break;
			case Right:
				x += n;
				break;
		}
	}
	
	/**
	 * Set the x value of the coordinate.
	 * 
	 * @param x X value to set coordinate to.
	 */
	public void setX(int x){
		this.x = x;
	}
	
	/**
	 * Set the y value of the coordinate.
	 * 
	 * @param y Y value to set coordinate to.
	 */
	public void setY(int y){
		this.y = y;
	}
	
	/**
	 * Get the Manhattan Distance (MD) from the
	 * current point to a goal point.
	 * 
	 * @return Manhattan Distance of point from goal.
	 */
	public double MD(Coordinate goal){
		return Math.abs(x-goal.x()) + Math.abs(y-goal.y());
	}
	
	/**
	 * Clone the coordinate.
	 */
	@Override
	public Coordinate clone() {
		Coordinate clone = new Coordinate(this.x, this.y);
		return clone;
	}

	/**
	 * Hash code implementation for coordinate. Ensures
	 * that shallowly-equal coordinates are hashed to 
	 * the same value.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	/**
	 * Equals code implementation for coordinate. Ensures
	 * that shallowly-equal coordinates are reported as
	 * equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Coordinate other = (Coordinate) obj;
		if (x != other.x) return false;
		if (y != other.y) return false;
		return true;
	}
}
