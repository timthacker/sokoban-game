
/**
 * This enum defines possible states for a tile.
 */
public enum Tile {
	Wall,
	Empty,
	Box,
	Hole, // a.k.a. the 'goal'
	BoxInHole,
	Player,
	PlayerOnHole;

	/**
	 * Convert the state of a tile to a string.
	 */
	public String toString() {
		switch(this) {
			case Wall:
				return "W";
			case Empty:
				return ".";
			case Box:
				return "B";
			case Player:
				return "P";
			case Hole:
				return "H";
			case BoxInHole:
				return "I";
			case PlayerOnHole:
				return "O";
		default:
			break;
		}
		return "";
	}
}
