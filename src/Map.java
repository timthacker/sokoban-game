import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Map implements Cloneable {
	/**
	 * Our map is represented as a 2D array.
	 * Each element is set to a particular
	 * value that indicates what is inside that
	 * square on the map (e.g. a wall, empty space,
	 * etc.)
	 *
	 * NOTE: Think of the grid as an x-y plane where
	 * coordinate (x,y) = (0,0) is the top left.
	 * Traveling to right = increasing x
	 * Traveling down = increasing y
	 * I.e:
	 *		[[0,0], [0,1], ...]
	 *		[[1,0], [1,1], ...]
	 *		[[2,0], [2,1], ...]
	 *		...
	 */
	private Tile grid[][];
	private int width;
	private int height;
	private int initialNumBoxes;
	private int remainingNumBoxes;
	private HashSet<Coordinate> boxPositions;
	private HashSet<Coordinate> goalPositions;
	private Player player;
	private MovementNotifier notify;
	private List<Direction> sol;
	private int expanded;
	
	/**
	 * Construct a new Map with a player set in a given position.
	 *
	 * @param difficulty the difficulty of the map.
	 */
	Map(Difficulty difficulty) {
		// Generate a map with a set difficulty
		this.width = difficulty.getWidth();
		this.height = difficulty.getHeight();
		this.initialNumBoxes = difficulty.getNumBoxes();
		this.remainingNumBoxes = 0;
		this.boxPositions = new HashSet<Coordinate>();
		this.goalPositions = new HashSet<Coordinate>();
		this.grid = new Tile[height][width];

		this.initWorld();
		notify = null;
	}

	/**
	 * Creates a clone of another map.
	 * i.e. create a map given a grid and a player. 
	 * 
	 * @param grid Layout of the map.
	 * @param numBoxes Number of boxes in the map.
	 * @param player Player on the map.
	 * @param boxPositions Positions of boxes on the map.
	 * @param goalPositions Positions of goals on the map.
	 * @param sol Solution for the map.
	 */
	Map(Tile grid[][], int numBoxes, Player player, HashSet<Coordinate> boxPositions, 
			HashSet<Coordinate> goalPositions, List<Direction> sol) {
		this.grid = grid.clone();
		this.initialNumBoxes = numBoxes;
		this.remainingNumBoxes = numBoxes;
		this.boxPositions = new HashSet<Coordinate>(boxPositions);
		this.goalPositions = new HashSet<Coordinate>(goalPositions);
		this.height = grid.length;
		if (this.height != 0) this.width = grid[0].length;
		else this.width = 0;
		this.player = player;
		notify = null;
		this.sol = sol;
		expanded = -1;
	}
	
	/**
	 * Method to generate a valid map.
	 * Will call the map generator class to generate a solvable
	 * map.
	 */
	public void initWorld() {
		MapGenerator mapGenerator = new MapGenerator(this);
		mapGenerator.getNewMapGrid();
	}

	/**
	 * Do an Astar search for the current map.
	 * Update the variables for the solution and 
	 * number of nodes expanded.
	 * @return
	 */
	public boolean findSolution() {
		Astar a = new Astar(this);
		sol = a.getSolution();
		expanded = a.getNumExpanded();
		return a.getSolutionFound();
	}

	/**
	 * Get the solution to the current map if 
	 * findSolution has been run. Otherwise returns
	 * null.
	 * 
	 * @return Solution to current map if findSolution has been
	 * 			run. Otherwise null.
	 */
	public List<Direction> getSol(){
		return sol;
	}
	
	/**
	 * Add a box to the map at a coordinate. 
	 * 
	 * @param c Coordinate to add box.
	 */
	public void addBox(Coordinate c){
		grid[c.y()][c.x()] = Tile.Box;
		boxPositions.add(c);
		remainingNumBoxes++;
	}

	/**
	 * Add a goal to the map at a coordinate.
	 * 
	 * @param c Coordinate to add the map.
	 */
	public void addGoal(Coordinate c){
		grid[c.y()][c.x()] = Tile.Hole;
		goalPositions.add(c);
	}

	/**
	 * Set the number of remaining boxes to be placed
	 * into a goal.
	 * 
	 * @param i Number of boxes remaining to be placed into a goal.
	 */
	public void setRemainingNumBoxes(int i){
		remainingNumBoxes = i;
	}


	/**
	 * Add a movement notifier to the map.
	 * 
	 * @param n Movement notifier to add.
	 */
	public void addMovementNotifier(MovementNotifier n) {
		notify = n;
	}

	/**
	 * Move the player in a given direction, if possible.
	 * Also update the map accordingly
	 *
	 * @param d Direction to move player.
	 */
	public boolean movePlayer(Direction d) {
		// Generate a temporary new player position in the desired direction
		Coordinate oldPlayerPos = player.getPos();
		Coordinate newPlayerPos = new Coordinate(player.getPos().x(), player.getPos().y());
		newPlayerPos.Move(d);

		boolean moveSuccess = false;

		if (grid[newPlayerPos.y()][newPlayerPos.x()] == Tile.Empty){
			// player is moving into empty space
			moveSuccess = true;
			grid[newPlayerPos.y()][newPlayerPos.x()] = Tile.Player;
		} else if (grid[newPlayerPos.y()][newPlayerPos.x()] == Tile.Hole){
			// player is moving into empty hole
			moveSuccess = true;
			grid[newPlayerPos.y()][newPlayerPos.x()] = Tile.PlayerOnHole;
		} else if (grid[newPlayerPos.y()][newPlayerPos.x()] == Tile.Box ||
				grid[newPlayerPos.y()][newPlayerPos.x()] == Tile.BoxInHole){
			Coordinate oldBoxPos = new Coordinate(newPlayerPos.x(), newPlayerPos.y()); // The original box position is at the new player position
			Coordinate newBoxPos = oldBoxPos.clone();
			newBoxPos.Move(d);
			// player is moving into a box or a boxInHole

			if (grid[newBoxPos.y()][newBoxPos.x()] == Tile.Empty){
				moveSuccess = true;
				if (grid[newPlayerPos.y()][newPlayerPos.x()] == Tile.Box){
					// box is moving from box to empty space
					boxPositions.remove(newPlayerPos);
					boxPositions.add(newBoxPos);
					grid[newPlayerPos.y()][newPlayerPos.x()] = Tile.Player;
					grid[newBoxPos.y()][newBoxPos.x()] = Tile.Box;
					if(notify != null) {
						notify.moveBox(d, oldBoxPos, Tile.Empty, Tile.Box);
					}
				} else{
					// box is moving from boxInHole to empty space
					this.remainingNumBoxes++;
					boxPositions.add(newBoxPos);
					grid[newPlayerPos.y()][newPlayerPos.x()] = Tile.PlayerOnHole;
					grid[newBoxPos.y()][newBoxPos.x()] = Tile.Box;
					if(notify != null) {
						notify.moveBox(d, oldBoxPos, Tile.Hole, Tile.Box);
					}
				}
			} else if (grid[newBoxPos.y()][newBoxPos.x()] == Tile.Hole){
				// box is moving into a hole
				moveSuccess = true;
				if (grid[newPlayerPos.y()][newPlayerPos.x()] == Tile.BoxInHole){
					// player is moving box from boxInHole to boxInHole
					grid[newPlayerPos.y()][newPlayerPos.x()] = Tile.PlayerOnHole;
					grid[newBoxPos.y()][newBoxPos.x()] = Tile.BoxInHole;
					if(notify != null) {
						notify.moveBox(d, oldBoxPos, Tile.Hole, Tile.BoxInHole);
					}
				} else{
					// player is moving box from box to boxInHole
					this.remainingNumBoxes--;
					boxPositions.remove(newPlayerPos);
					grid[newPlayerPos.y()][newPlayerPos.x()] = Tile.Player;
					grid[newBoxPos.y()][newBoxPos.x()] = Tile.BoxInHole;
					if(notify != null) {
						notify.moveBox(d, oldBoxPos, Tile.Empty, Tile.BoxInHole);
					}
				}
			}
		}

		// Update the player's old position
		if (moveSuccess){
			player.setDir(d);
			player.setPos(newPlayerPos);
			if (grid[oldPlayerPos.y()][oldPlayerPos.x()] == Tile.Player){
				// if old tile was player, clear it
				grid[oldPlayerPos.y()][oldPlayerPos.x()] = Tile.Empty;
				if(notify != null) {
					notify.movePlayer(d, oldPlayerPos, Tile.Empty);
				}
			} else if (grid[oldPlayerPos.y()][oldPlayerPos.x()] == Tile.PlayerOnHole){
				// if old tile was player standing on a hole, replace with hole
				grid[oldPlayerPos.y()][oldPlayerPos.x()] = Tile.Hole;
				if(notify != null) {
					notify.movePlayer(d, oldPlayerPos, Tile.Hole);
				}
			}
		}
		return moveSuccess;
	}
	
	/**
	 * Check if the player can move in a given direction.
	 * 
	 * @param d Direction to check if player can move in.
	 * 
	 * @return True if player can move, false otherwise.
	 */
	private boolean canMove(Direction d){
		// Get the new coordinate
		Coordinate newPos = new Coordinate(player.getPos().x(), player.getPos().y());
		newPos.Move(d);

		Tile currTile = grid[newPos.y()][newPos.x()];
		if (currTile == Tile.Wall || currTile == Tile.Player || currTile == Tile.PlayerOnHole){
			return false;
		} else if (currTile == Tile.Empty || currTile == Tile.Hole){
			return true;
		} else{
			// This is the case where player is colliding with
			// box or boxInHole
			newPos.Move(d); // simulate box movement
			currTile = grid[newPos.y()][newPos.x()];

			if (currTile == Tile.Empty || currTile == Tile.Hole){
				return true;
			} else{
				return false;
			}
		}
	}

	/**
	 * Clone the current grid.
	 * 
	 * @return A cloned version of the current grid.
	 */
	public Tile[][] cloneGrid() {
		Tile[][] newGrid = new Tile[this.height][this.width];
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				newGrid[y][x] = grid[y][x];
			}
		}
		return newGrid;
	}

	/**
	 * Get all possible moves that the player can make
	 * from their current position. Only valid moves
	 * will be returned (e.g. if move results in running
	 * into a wall, it will not be returned).
	 * 
	 * @return A list of valid moves from the player's current
	 *		  position.
	 */
	public List<Direction> getPossibleMoves(){
		List<Direction> moves = new ArrayList<Direction>();
		if (canMove(Direction.Up)) moves.add(Direction.Up);
		if (canMove(Direction.Down)) moves.add(Direction.Down);
		if (canMove(Direction.Left)) moves.add(Direction.Left);
		if (canMove(Direction.Right)) moves.add(Direction.Right);
		return moves;
	}

	/**
	 * Get all neighbours to a coordinate. The neighbours will
	 * be valid squares of the map, and will not be outside of the
	 * map's width/height.
	 * 
	 * @param c Coordinate to find neighbours of.
	 * 
	 * @return A list of neighbours to the given coordinate.
	 */
	public List<Coordinate> getNeighbours(Coordinate c){
		List<Coordinate> neighbours = new ArrayList<Coordinate>();

		int x = c.x();
		int y = c.y();

		if (x-1 >= 0) neighbours.add(new Coordinate(x-1, y));
		if (y-1 >= 0) neighbours.add(new Coordinate(x, y-1));
		if (x+1 < width) neighbours.add(new Coordinate(x+1, y));
		if (y+1 < height) neighbours.add(new Coordinate(x, y+1));

		return neighbours;
	}

	/**
	 * Get the map's grid.
	 * 
	 * @return The map's grid.
	 */
	public Tile[][] getGrid() {
		return grid;
	}

	/**
	 * Set the map's grid.
	 * 
	 * @param grid The grid to set the map's grid to.
	 */
	public void setGrid(Tile[][] grid) {
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				this.grid[y][x] = grid[y][x];
			}
		}
	}

	
	/**
	 * Get the width of the map.
	 * 
	 * @return The width of the map.
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Get the height of the map.
	 * 
	 * @return The height of the map.
	 */
	public int getHeight() {
		return height;
	}

	
	/**
	 * Get the number of boxes that were present
	 * on the map before the game had begun.
	 * 
	 * @return Number of boxes on map before game began.
	 */
	public int getInitialNumBoxes(){
		return initialNumBoxes;
	}

	/**
	 * Get the number of remaining boxes that have
	 * not been placed into goal squares yet.
	 * 
	 * @return Number of unsolved boxes.
	 */
	public int getRemainingNumBoxes(){
		return remainingNumBoxes;
	}

	/**
	 * Get a hash set of coordinates for boxes in the map.
	 *
	 * @return Hash set of box locations (coordinates).
	 */
	public HashSet<Coordinate> getBoxes(){
		return boxPositions;
	}

	/**
	 * Get a hash set of goals for boxes in the map.
	 *
	 * @return Hash set of goal locations (coordinates).
	 */
	public HashSet<Coordinate> getGoals(){
		return goalPositions;
	}

	/**
	 * Get the player associated with the map.
	 *
	 * @return Player located on the map.
	 */
	public Player getPlayer(){
		return player;
	}

	/**
	 * Set the map's player to a given player.
	 * 
	 * @param player Player to set map's player to.
	 */
	public void setPlayer(Player player){
		this.player = player;
	}

	/**
	 * Get the tile at a particular (x,y) coordinate.
	 * 
	 * @param x X coordinate of tile.
	 * @param y Y coordinate of tile.
	 * @return Tile at location X = x, Y = y.
	 */
	public Tile getTile(int x, int y) {
		return grid[y][x];
	}
	
	/**
	 * Clone the current map.
	 */
	@Override
	public Map clone() {
		Map ret = new Map(this.cloneGrid(),
							this.getRemainingNumBoxes(),
							this.getPlayer().clone(),
							this.getBoxes(),
							this.getGoals(), 
							this.getSol());
		return ret;
	}
	
	/**
	 * Ensure that shallowly-equal maps are hashed
	 * to the same value.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(grid);
		return result;
	}

	/**
	 * Ensure that shallowly-equal maps are
	 * reported as equal.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Map other = (Map) obj;
		if (!Arrays.deepEquals(grid, other.grid)) return false;
		return true;
	}
}
