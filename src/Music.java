import javax.sound.sampled.*;
import java.io.*;

public class Music {
	private Thread background;
	
	/**
	 * Constructor for Music class
	 * Calls playBackgroundMusic()
	 */
	
	Music() {
		playBackgroundTrack();
	}
	
	/**
	 * Start playing the background music and continues on a loop
	 */
	
	private void playBackgroundTrack() {
		background = new Thread() {
			@Override
			public void run() {
				try {
					File file = new File("src/audio/soundtrack.wav");
					Clip clip = AudioSystem.getClip();
					clip.open(AudioSystem.getAudioInputStream(file));
					FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
					gainControl.setValue(-5.0f); // Reduce volume by 10 decibels.
					clip.loop(Clip.LOOP_CONTINUOUSLY);
					Thread.sleep(clip.getMicrosecondLength());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		};
		
		background.start();
	}
	
	/**
	 * Plays a given audio file
	 * Plays in a new thread to not block the main program
	 * 
	 * @param sound file to start playing
	 */
	
	public void playSound(String sound) {
		new Thread() {
			@Override
			public void run() {
				try {
					File file = new File("src/audio/" + sound + ".wav");
					Clip clip = AudioSystem.getClip();
					clip.open(AudioSystem.getAudioInputStream(file));
					clip.start();
					//clip.loop(Clip.LOOP_CONTINUOUSLY);
					Thread.sleep(clip.getMicrosecondLength());

				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}.start();
	}
}
