import java.util.Comparator;

/**
 * Class for comparing two states.
 *
 */
public class FScoreComparator implements Comparator<State>{
	/**
	 * Compares two states.
	 */
	public int compare(State start, State end){
		// calculate total score (g + h) for both states
		int startFScore = start.getGScore()+start.getHScore();
		int endFScore = end.getGScore()+end.getHScore();
		
		// compare based on total score (fScore)
		if (startFScore < endFScore) return -1;
		else if (startFScore > endFScore) return 1;
		else return 0;
	}
}
