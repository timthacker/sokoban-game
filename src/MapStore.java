import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;

public class MapStore extends Thread {
	public static final int NUM_MAPS = 5;
	BlockingQueue<Map> next;
	Difficulty d;
	boolean cancel;

	MapStore(Difficulty d) {
		cancel = false;
		this.d = d;
		next = new ArrayBlockingQueue<Map>(NUM_MAPS);
	}

	public void run() {
		try {
			while(cancel == false) {
				Map add = new Map(d);
				next.put(add);
			}
		} catch(InterruptedException e) {}
	}
	
	public void cancel() {
		cancel = true;
	}
	
	public Map getNext() {
		try {
			return next.take();
		} catch(InterruptedException e) {
			return new Map(d);
		}
	}
}
